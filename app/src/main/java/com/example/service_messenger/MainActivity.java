package com.example.service_messenger;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private Messenger myService = null;
    private ServiceConnection myConnection;
    private boolean isBound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myConnection = new ServiceConnection() {
                    public void onServiceConnected(
                            ComponentName className,
                            IBinder service) {
                        myService = new Messenger(service);
                        isBound = true;
                        Log.e("prova", "service app connected");

                    }

                    public void onServiceDisconnected(
                            ComponentName className) {
                        Log.e("prova", "service app disconnected");
                        myService = null;
                        isBound = false;
                    }
                };
    }

    @Override
    protected void onStart() {
        Intent intent = new Intent(this, BackgroundSoundService.class);
        bindService(intent, myConnection, Context.BIND_AUTO_CREATE);
        super.onStart();
    }

    @Override
    protected void onStop() {
        if(isBound) {
            Log.e("prova", "unbinding");
            unbindService(myConnection);
            isBound = false;
        }
        super.onStop();
    }

    public void sendMessage()
    {
        if (!isBound){
            Log.e("prova", "service not bound");
            return;
        }
        Message msg = Message.obtain();
        Bundle bundle = new Bundle();
        bundle.putInt("volume", 100);
        msg.setData(bundle);
        try {
            myService.send(msg);
            Log.e("prova", "message sent");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


}
