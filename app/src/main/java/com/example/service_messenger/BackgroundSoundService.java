package com.example.service_messenger;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

public class BackgroundSoundService extends Service {

    private static final String TAG = null;
    private MediaPlayer player;
    private final Messenger myMessenger = new Messenger(new IncomingHandler());

    @Override
    public IBinder onBind(Intent intent) {
        return myMessenger.getBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Uri tono = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        player= MediaPlayer.create(this, tono );
        player.setLooping(true); // Set looping

    }
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }


    public IBinder onUnBind(Intent arg0) {
        // TO DO Auto-generated method
        return null;
    }

    public void onStop() {

    }
    public void onPause() {

    }
    @Override
    public void onDestroy() {
        player.stop();
        player.release();
    }

    @Override
    public void onLowMemory() {

    }
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            Bundle data = msg.getData();
            int volume = data.getInt("volume");
            Log.e("prova", "volume " +  volume);
            player.setVolume(volume, volume);
            player.start();
        }
    }
}